import React from 'react';
// import logo from './logo.svg';
// import './App.css';
import Table from './components/Table'


class App extends React.Component {
  state = {
    table_main_headers:['Email Subject',	'Outbound',	'','Inbound',	'','Emails',	'Duration',	'Overall Health'],
    // table_main_row:[['Re: Project Presentation ','25 Emails over 3 Months'],],
    table_data:[
      // [
        [{content:'Re: Project Presentation',element:'', eleClsName:'brllr-t-copy'},
          {content:'25 Emails over 3 Months',element:'', eleClsName:'brllr-t-subtext'}],//1st row
          
          {content:'',element:'img', eleClsName:'',parentClsName:'avatar'},
          
          [{content:'',element:'', eleClsName:'brllr-tonetag confident'},
          {content:'',element:'', eleClsName:'brllr-tonetag analytical'}],

          {content:'',element:'img', eleClsName:'',parentClsName:'avatar'},  

          [{content:'',element:'', eleClsName:'brllr-tonetag tentative'},
          
          {content:'',element:'', eleClsName:'brllr-tonetag anger'}],

          {content:'25',element:'td', eleClsName:'brllr-t-data sm'},

          {content:'3mo',element:'td', eleClsName:'brllr-t-data sm'},

          {content:'25%',element:'td', eleClsName:'brllr-t-data sm health neg'},

      ],      

    // ],
    
    
  
    
  }

  render(){
    
    return (
      <div className="App">
        <Table tmh={this.state.table_main_headers} td={this.state.table_data}/>
      </div>
    );
  }
}

export default App;
