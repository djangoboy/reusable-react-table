import React from 'react'
import UserCurrent from './../images/user-current.png'
import UserHenry from './../images/user-henry.png'
import '.././css/style.css'

const Table=(props)=> {

  const tableMainHeader = props.tmh.map(header=>{
      return (
        <td>{header}</td>   
      )
  })


  const generate_jsx_for_col =(td)=>{
    //this will return appropriate jsx
    if(td.element==='div'){
      console.log('yes this is div')
      return(
        <td><div className={td.eleClsName?(td.eleClsName):(null)}>{td.content}</div></td>
      )
    }
    if(td.element===''){
      console.log('empty elment')
      return(
        <div className={td.eleClsName?(td.eleClsName):(null)}>{td.content}</div>
      )
    }
    if (td.element==='img') {
      console.log('yes this is img')
      return(
        <td className={td.parentClsName?(td.parentClsName):null}><img src={UserHenry} alt="no logo"/></td>
      )
    }
    if (td.element==='p') {
      console.log('yes this is img')
      return(
        <td><p className={td.eleClsName?(td.eleClsName):(null)}>{td.content}</p></td>
      )
    }
    if (td.element==='b') {
      return(
        <td><b className={td.eleClsName?(td.eleClsName):(null)}>{td.content}</b></td>
      )
    }

    // if (td.element==='td') {
    if (td.element===undefined || 'td') {
      console.log('td ran',td)
      return(
        <td className={td.eleClsName?(td.eleClsName):(null)}>{td.content}</td>
      )
    }

    // else {
    //   return(
    //     <div className={td.eleClsName?(td.eleClsName):(null)}>{td.content}</div>
    //   )
    // }
  }

  // const dummy_data = {content:'Re: Project Presentation1',element:'div', eleClsName:'brllr-t-copy'}
  
  // const dummy_data_list = [[{content:'Re: Project Presentation2',element:'div', eleClsName:'brllr-t-copy'},{content:'Re: Project Presentation2',element:'div', eleClsName:'brllr-t-copy'}],{content:'Re: Project Presentation3',element:'img', eleClsName:'brllr-t-copy'}]
            
  const dummy_data_list=props.td
  
  const data_list = dummy_data_list.map((dummy_data)=>{
  //if dummy is a list then process dummy data using map one more time.
  //dummy_data.length>0 and typeof(dummy_data)==oject ==>array 
  //if a.length ==undefined ==> dict

    if(dummy_data.length === undefined){
      console.log('if got hit..')
      return(
        generate_jsx_for_col(dummy_data)
    )
    }
    else{
      console.log('else got hit..')
      const dummy_d = dummy_data.map((dummy_d)=>{
        //or remove td from dummy_d
        console.log('dummy_D here',dummy_d)
        return (
          generate_jsx_for_col(dummy_d)
          //write something similer to it but only return everything in one <td></td>     generate_jsx_for_col(dummy_d)
        )
      })

      return <td>{dummy_d}</td>
    }
  })

  // console.log(data_list,'data_list')
  // const a = generate_jsx_for_col(dummy_data)
  // console.log(a,'hereisa')
  return (
    <div>
      <table>
      <thead>
        <tr>
            {tableMainHeader}
        </tr>
    </thead>
    <tbody>
      <tr>
        {data_list}
      </tr>      
    </tbody>
      </table>
    </div>
  )
}

export default Table
